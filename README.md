## Gulp自动更新版本号

### 安装依赖
```bash
# 全局安装gulp命令
npm install gulp -g
# 本地安装gulp,gulp-rev,gulp-rev-collector
npm install gulp@3.9.1 gulp-rev@8.0.0 gulp-rev-collector@1.1.1
```
### 修改配置
- node_modules\gulp-rev\index.js
```javascript
/*( line 134 ) =>*/
// manifest[originalFile] = revisionedFile;
manifest[originalFile] = originalFile + '?v=' + file.revHash;
```

- node_modules\rev-path\index.js
```javascript
/*( line 10 )=>*/
//return filename + '-' + hash + ext;
return filename + ext;
```

- node_modules\gulp-rev-collector\index.js
```javascript
/*(line 31~33)=>*/
// if ( !_.isString(json[key]) || path.basename(json[key]).replace(new RegExp( opts.revSuffix ), '' ) !==  path.basename(key) ) {
//     isRev = 0;
// }
if ( !_.isString(json[key]) || path.basename(json[key]).split('?')[0] !== path.basename(key) ) {
    isRev = 0;
}

/*(line 46)=>*/
// return pattern.replace(/[\-\[\]\{\}\(\)\*\+\?\.\^\$\|\/\\]/g, "\\$&");
var rp = pattern.replace(/[\-\[\]\{\}\(\)\*\+\?\.\^\$\|\/\\]/g, "\\$&");
rp = pattern + "(\\?v=(\\d|[a-z]){8,10})*";
return rp;

/*(line 92)=>*/
// patterns.push(escPathPattern((path.dirname(key) === '.' ? '' : closeDirBySep(path.dirname(key))) + path.basename(key, path.extname(key))) +
//     opts.revSuffix +
//     escPathPattern(path.extname(key))
// );
patterns.push( escPathPattern( (path.dirname(key) === '.' ? '' : closeDirBySep(path.dirname(key)) ) + path.basename(key, path.extname(key)) )
    + opts.revSuffix
    + escPathPattern( path.extname(key) ) + "(\\?v=(\\d|[a-z]){8,10})*"
);
```

### 参数
参见gulpfile.js

### 运行
```bash
glupdemo|master⚡ ⇒ gulp rev
[14:14:37] Using gulpfile ~/Documents/work/tmp/glupdemo/gulpfile.js
[14:14:37] Starting 'css'...
[14:14:37] Starting 'js'...
[14:14:37] Finished 'css' after 53 ms
[14:14:37] Finished 'js' after 46 ms
[14:14:37] Starting 'rev'...
[14:14:37] Finished 'rev' after 9.89 ms
```

### 注意事项